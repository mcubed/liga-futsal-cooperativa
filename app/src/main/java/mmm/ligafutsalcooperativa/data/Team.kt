package mmm.ligafutsalcooperativa.data

import com.ptrbrynt.firestorelivedata.FirestoreModel

data class Team(
    var position: Int = 0,
    var name: String = "",
    var color: String = "",
    var draws: Int = 0,
    var wins: Int = 0,
    var loses: Int = 0,
    var proGoals: Int = 0,
    var contraGoals: Int = 0,
    var points: Int = 0
): FirestoreModel() {

    fun updateGame(proGoals : Int, contraGoals: Int) {
        when {
            proGoals > contraGoals -> {
                this.wins += 1
                this.points += 4
            }
            proGoals < contraGoals -> {
                this.loses += 1
                this.points += 1
            }
            else -> {
                this.draws += 1
                this.points += 2
            }
        }
        this.contraGoals += contraGoals
        this.proGoals += proGoals
    }
}