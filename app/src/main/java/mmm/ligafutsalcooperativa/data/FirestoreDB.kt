package mmm.ligafutsalcooperativa.data

class FirestoreCollections {

    companion object {
        const val TEAMS = "team"
        const val GAMES = "game"
    }
}
class FirestoreFields {

    companion object {
        //team
        const val TEAM1 = "team1"
        const val TEAM2 = "team2"
        const val DATE = "date"
        const val GOALS1 = "goals1"
        const val GOALS2 = "goals1"

        //game
        const val POINTS = "points"
        const val NAME = "name"
        const val WINS = "wins"
        const val LOSES = "loses"


    }
}