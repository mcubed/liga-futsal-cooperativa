package mmm.ligafutsalcooperativa.data

import com.ptrbrynt.firestorelivedata.FirestoreModel
import mmm.ligafutsalcooperativa.utils.dateFormat
import java.util.*

data class Game(
    val team1: String = "",
    val team2: String = "",
    var goals1: Int? = null,
    var goals2: Int? = null,
    val date: Date? = null,
    val field: String= ""
): FirestoreModel()
{

    fun getResult() : String {
        return if (goals1 != null && goals2 != null) {
            "$goals1 - $goals2"
        }
        else {
            val cal = Calendar.getInstance()
            cal.time = date
            String.format("%02d:%02d", cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE))

        }
    }

    val dateText : String
        get() {
            return dateFormat.format(date)
        }
}