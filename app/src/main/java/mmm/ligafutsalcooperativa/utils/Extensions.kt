package mmm.ligafutsalcooperativa.utils

import mmm.ligafutsalcooperativa.data.Game
import mmm.ligafutsalcooperativa.data.Team
import java.text.SimpleDateFormat
import java.util.*

var dateFormat = SimpleDateFormat("EEEE dd/MM", Locale.getDefault())

fun List<Game>?.withHeaders(): List<Any> {
    val result = mutableListOf<Any>()
    this?.forEach{
        val date = dateFormat.format(it.date)
        result.addAll(
            when(date){
                !in result -> listOf(date, it)
                else -> listOf(it)
            }
        )
    }
    return result
}

fun List<Team>?.withPositions() : List<Team>{
    this?.forEach {
        it.position = this.indexOf(it) + 1
    }
    return this!!
}