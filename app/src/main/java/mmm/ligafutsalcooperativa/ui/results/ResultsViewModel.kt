package mmm.ligafutsalcooperativa.ui.results

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.ptrbrynt.firestorelivedata.QueryLiveData
import com.ptrbrynt.firestorelivedata.asLiveData
import mmm.ligafutsalcooperativa.data.FirestoreCollections
import mmm.ligafutsalcooperativa.data.FirestoreFields
import mmm.ligafutsalcooperativa.data.Game

class ResultsViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is notifications Fragment"
    }
    val text: LiveData<String> = _text

    private val db = FirebaseFirestore.getInstance()

    var response: QueryLiveData<Game>

    init{
        response = db.collection(FirestoreCollections.GAMES)
            .orderBy(FirestoreFields.DATE, Query.Direction.DESCENDING)
            .asLiveData()
    }
}