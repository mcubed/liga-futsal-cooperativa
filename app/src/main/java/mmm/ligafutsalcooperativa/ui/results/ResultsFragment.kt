package mmm.ligafutsalcooperativa.ui.results

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.github.nitrico.lastadapter.LastAdapter
import com.ptrbrynt.firestorelivedata.ResourceObserver
import kotlinx.android.synthetic.main.fragment_table.view.*
import mmm.ligafutsalcooperativa.BR
import mmm.ligafutsalcooperativa.R
import mmm.ligafutsalcooperativa.data.Game

class ResultsFragment : Fragment() {

    private lateinit var resultsViewModel: ResultsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        resultsViewModel =
            ViewModelProviders.of(this).get(ResultsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_results, container, false)


        resultsViewModel.response.observe(this, object: ResourceObserver<List<Game>> {
            override fun onSuccess(data: List<Game>?) {
                data?.let { games ->
                    LastAdapter(games.filter { it.goals1 != null }, BR.game)
                        .map<Game>(R.layout.item_game)
                        .into(root.recycler)
                }

            }

            override fun onError(throwable: Throwable?, errorMessage: String?) {
                Log.e(throwable?.localizedMessage, errorMessage)
            }

            override fun onLoading() {
                // Handle loading state e.g. display a loading animation
            }

        })
        return root
    }
}