package mmm.ligafutsalcooperativa.ui.table

import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.ptrbrynt.firestorelivedata.QueryLiveData
import com.ptrbrynt.firestorelivedata.asLiveData
import mmm.ligafutsalcooperativa.data.FirestoreCollections
import mmm.ligafutsalcooperativa.data.FirestoreFields
import mmm.ligafutsalcooperativa.data.Team

class TableViewModel : ViewModel() {

    private val db = FirebaseFirestore.getInstance()

    val response: QueryLiveData<Team>

    init {
        response = db.collection(FirestoreCollections.TEAMS).orderBy(FirestoreFields.POINTS, Query.Direction.DESCENDING).asLiveData()
    }

    private fun restoreDataBase(){
        db.collection("team").get().addOnSuccessListener { snapshot ->
            snapshot.documents.forEach {
                db.collection("team").document(it.id).delete()
            }
        }

        val teams = arrayListOf<Team>()

        teams.add(Team(name = "Loz Patoz"))
        teams.add(Team(name = "C.T.G"))
        teams.add(Team(name = "Hermanos Carabanchel"))
        teams.add(Team(name = "Rumanadas"))
        teams.add(Team(name = "Trasto Team"))
        teams.add(Team(name = "Trinches"))
        teams.add(Team(name = "Tancitaro"))
        teams.add(Team(name = "Esfinter de Milán"))
        teams.add(Team(name = "Paralimpiakos"))
        teams.add(Team(name = "Realmente"))
        teams.add(Team(name = "Transister City"))
        teams.add(Team(name = "Poderosos Bastardos"))
        teams.add(Team(name = "Columbus Crew"))

        teams.forEach {
            db.collection("team").add(it)
        }
    }

}