package mmm.ligafutsalcooperativa.ui.table

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.github.nitrico.lastadapter.LastAdapter
import com.ptrbrynt.firestorelivedata.ResourceObserver
import kotlinx.android.synthetic.main.fragment_table.view.recycler
import mmm.ligafutsalcooperativa.BR
import mmm.ligafutsalcooperativa.R
import mmm.ligafutsalcooperativa.data.Team
import mmm.ligafutsalcooperativa.utils.withPositions

class TableFragment : Fragment() {

    private lateinit var tableViewModel: TableViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        tableViewModel = ViewModelProviders.of(this).get(TableViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_table, container, false)

        tableViewModel.response.observe(this, object: ResourceObserver<List<Team>> {
            override fun onSuccess(data: List<Team>?) {

                val sortedList = data?.sortedWith(
                    compareByDescending<Team> { it.points }
                        .thenByDescending { it.proGoals.minus(it.contraGoals)}
                )

                LastAdapter(sortedList.withPositions(), BR.team)
                    .map<Team>(R.layout.item_team)
                    .into(root.recycler)
            }

            override fun onError(throwable: Throwable?, errorMessage: String?) {
                Log.e(throwable?.localizedMessage, errorMessage)
            }

            override fun onLoading() {
                // Handle loading state e.g. display a loading animation
            }

        })

        return root
    }
}
