package mmm.ligafutsalcooperativa.ui.games

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.github.nitrico.lastadapter.LastAdapter
import com.github.nitrico.lastadapter.Type
import com.google.android.material.textfield.TextInputEditText
import com.ptrbrynt.firestorelivedata.ResourceObserver
import kotlinx.android.synthetic.main.fragment_games.view.*
import mmm.ligafutsalcooperativa.BR
import mmm.ligafutsalcooperativa.R
import mmm.ligafutsalcooperativa.data.Game
import mmm.ligafutsalcooperativa.databinding.GameResultDialogBinding
import mmm.ligafutsalcooperativa.databinding.ItemGameBinding
import mmm.ligafutsalcooperativa.databinding.NewGameDialogBinding
import mmm.ligafutsalcooperativa.ui.info.InfoActivity
import mmm.ligafutsalcooperativa.utils.withHeaders
import java.util.*
import java.text.SimpleDateFormat


class GamesFragment : Fragment() {

    private lateinit var gamesViewModel: GamesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        gamesViewModel = ViewModelProviders.of(this).get(GamesViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_games, container, false)

        gamesViewModel.response.observe(this, object: ResourceObserver<List<Game>> {
            override fun onSuccess(data: List<Game>?) {

                if(data.isNullOrEmpty()){
                    root.recycler.visibility = View.GONE
                    root.no_games_text.visibility = View.VISIBLE
                } else {
                    root.recycler.visibility = View.VISIBLE
                    root.no_games_text.visibility = View.GONE
                    LastAdapter(data.withHeaders(), BR.game)
                        .map<String>(R.layout.item_date)
                        .map<Game>(Type<ItemGameBinding>(R.layout.item_game)
                            .onClick { gameOptionsDialog(it.binding.game).show() })
                        .into(root.recycler)
                }
            }

            override fun onError(throwable: Throwable?, errorMessage: String?) {
                Log.e(throwable?.localizedMessage, errorMessage)
            }

            override fun onLoading() {
                // Handle loading state e.g. display a loading animation
            }

        })

        root.info.setOnClickListener {
            startActivity(Intent(context, InfoActivity::class.java))
        }

        val fab: View = root.findViewById(R.id.add_game_fab)
        fab.setOnClickListener { newGame() }

        return root
    }
    private fun gameOptionsDialog(game : Game?) : Dialog {
        val builder = AlertDialog.Builder(context)
        val inflater = LayoutInflater.from(context)
        val binding = DataBindingUtil.inflate<GameResultDialogBinding>(inflater, R.layout.game_result_dialog, null, false)
        binding.game = game
        builder.setView(binding.root)
        val dialog = builder.create()
        dialog.setButton(
            DialogInterface.BUTTON_POSITIVE,
            "Aceptar"
        ){_,_ ->
            if(validate(arrayOf(
                    binding.goals1,
                    binding.goals2
                )))
                gamesViewModel.setResult(
                    game,
                    binding.goals1.text.toString().toInt(),
                    binding.goals2.text.toString().toInt()
                )
        }

        dialog.setButton(
            DialogInterface.BUTTON_NEGATIVE,
            "Cancelar"
        ) { _, _ -> dialog.dismiss()}

        return dialog
    }

    private fun newGame() {
        val builder = AlertDialog.Builder(context)
        val binding = DataBindingUtil.inflate<NewGameDialogBinding>(LayoutInflater.from(context), R.layout.new_game_dialog, null, false)

        val selectedDate = Calendar.getInstance()
        val format = SimpleDateFormat.getDateTimeInstance()

        binding.team1Text.setOnClickListener {
            show(context!!, it)
        }

        binding.team2Text.setOnClickListener {
            show(context!!, it)
        }

        binding.dateText.setOnClickListener {
            showNativeCalendar(DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                selectedDate.set(year, month, dayOfMonth)
                showTimePicker(TimePickerDialog.OnTimeSetListener { _, hourOfDay, minutes ->
                    selectedDate.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    selectedDate.set(Calendar.MINUTE, minutes)
                    selectedDate.set(Calendar.SECOND, 0)
                    val date = selectedDate.time
                    binding.dateText.setText(format.format(date))
                })
            })
        }

        builder.setView(binding.root)
        val dialog = builder.create()

        dialog.setButton(
            DialogInterface.BUTTON_POSITIVE,
            "Aceptar"
        ) { _, _ -> }

        dialog.setButton(
            DialogInterface.BUTTON_NEGATIVE,
            "Cancelar"
        ) { _, _ -> }

        dialog.show()

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            if(validate(arrayOf(
                    binding.team1Text,
                    binding.team2Text,
                    binding.dateText,
                    binding.fieldText
                ))) {
                gamesViewModel.newGame(
                    Game(
                        binding.team1Text.text.toString(),
                        binding.team2Text.text.toString(),
                        null,
                        null,
                        selectedDate.time,
                        binding.fieldText.text.toString()
                    )
                )
                dialog.dismiss()
            }
        }
    }

    private fun validate(editTexts: Array<EditText>) : Boolean{
        var valid = true
        editTexts.forEach {
            if (it.text.isNullOrEmpty()) {
                it.error = "Rellena este campo"
                valid = false
            }
        }
        return valid
    }

    fun show(context: Context, textInput: View) {

        val teams = arrayListOf<String>()

        teams.add("Loz Patoz")
        teams.add("C.T.G")
        teams.add("Hermanos Carabanchel")
        teams.add("Rumanadas")
        teams.add("Trasto Team")
        teams.add("Trinches")
        teams.add("Tancitaro")
        teams.add("Esfinter de Milán")
        teams.add("Paralimpiakos")
        teams.add("Realmente")
        teams.add("Transister City")
        teams.add("Poderosos Bastardos")
        teams.add("Columbus Crew")

        val menu = PopupMenu(context, textInput)
        menu.setOnMenuItemClickListener { item ->
            (textInput as TextInputEditText).setText(teams[item.order])
            false
        }
        for (team in teams) {
            menu.menu.add(
                Menu.NONE,
                Menu.NONE,
                teams.indexOf(team),
                team
            )
        }
        menu.show()
    }

    private fun showNativeCalendar(listener: DatePickerDialog.OnDateSetListener) {
        val calendar = Calendar.getInstance()

        val datePickerDialog = DatePickerDialog(
            context!!, listener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
    }

    private fun showTimePicker(listener: TimePickerDialog.OnTimeSetListener) {
        val calendar = Calendar.getInstance()

        val dialog = TimePickerDialog(
            context, listener ,
            calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE),
            true
        )
        dialog.show()
    }


}


