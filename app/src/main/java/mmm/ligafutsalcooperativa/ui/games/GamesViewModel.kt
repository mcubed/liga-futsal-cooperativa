package mmm.ligafutsalcooperativa.ui.games

import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.ptrbrynt.firestorelivedata.QueryLiveData
import com.ptrbrynt.firestorelivedata.asLiveData
import mmm.ligafutsalcooperativa.data.FirestoreCollections
import mmm.ligafutsalcooperativa.data.FirestoreFields
import mmm.ligafutsalcooperativa.data.Game
import mmm.ligafutsalcooperativa.data.Team

class GamesViewModel : ViewModel() {

    private val db = FirebaseFirestore.getInstance()

    lateinit var response: QueryLiveData<Game>

    init {
        getGames()
    }
    private fun getGames(){
        response = db.collection(FirestoreCollections.GAMES)
            .orderBy(FirestoreFields.DATE, Query.Direction.ASCENDING)
            .whereEqualTo(FirestoreFields.GOALS1, null)
            .asLiveData()
    }

    fun newGame(game : Game){
        db.collection(FirestoreCollections.GAMES).add(game)
        getGames()
    }

    fun setResult(game : Game?, goals1 : Int, goals2 : Int) {
        game?.let {
            it.goals1 = goals1
            it.goals2 = goals2
            db.collection(FirestoreCollections.GAMES).document(game.id!!)
                .set(it)
                .addOnSuccessListener {
                    updateTeam(game.team1, game.goals1!!, game.goals2!!)
                    updateTeam(game.team2, game.goals2!!, game.goals1!!)
                }
        }
    }

    private fun updateTeam(team : String, proGoals : Int, contraGoals: Int){
        db.collection(FirestoreCollections.TEAMS)
            .whereEqualTo(FirestoreFields.NAME, team)
            .limit(1).get()
            .addOnSuccessListener { teamSnapshot ->
                val document = db.collection(FirestoreCollections.TEAMS)
                    .document(teamSnapshot.documents[0].id)

                document.get().addOnSuccessListener { snapShot ->
                    val team = snapShot.toObject(Team::class.java)
                    team?.let {
                        it.updateGame(proGoals, contraGoals)
                        document.set(it)
                    }
                }
            }
    }
}
