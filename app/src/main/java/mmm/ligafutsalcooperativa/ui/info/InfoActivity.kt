package mmm.ligafutsalcooperativa.ui.info

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.squareup.okhttp.internal.Version
import mehdi.sakout.aboutpage.AboutPage
import mehdi.sakout.aboutpage.Element
import mmm.ligafutsalcooperativa.R


class InfoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val aboutPage = AboutPage(this)
            .setDescription("Aplicación para la gestión de partidos de la liga cooperativa de fútbol sala de Madrid")
            .setImage(R.mipmap.ic_launcher)
            .addItem(Element("Version 1.0.0", null))
            .addItem(Element("Esta aplicación se encuentra en fase de pruebas, por favor, envia un correo si encuentras cualquier fallo", null))
            .addGroup("Dudas, sugerencias o errores:")
            .addEmail("mmm.droid.dev@gmail.com")
            .addWebsite("https://gitlab.com/mcubed/liga-futsal-cooperativa")
            .create()
        setContentView(aboutPage)
    }
}